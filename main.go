package main

import (
	"flag"
	"fmt"
	"time"
)

func main() {
	boxesFlag := flag.Int("boxes", 4, "Number of boxes")
	flag.Parse()
	boxes := *boxesFlag
	fmt.Println("Boxes:", boxes)
	start := time.Now()
	var solutions [][]int
	if boxes == 4 {
		solutions = append(solutions, []int{1, 2, 0, 1}, []int{2, 0, 2, 0})
	} else if boxes == 5 {
		solutions = append(solutions, []int{2, 1, 2, 0, 0})
	} else if boxes > 6 {
		solution := make([]int, boxes)
		solution[0] = boxes - 4
		solution[1] = 2
		solution[2] = 1
		solution[boxes-4] = 1
		solutions = append(solutions, solution)
	}

	fmt.Println("Solutions:", solutions)
	fmt.Println("Duration:", time.Since(start))

}
